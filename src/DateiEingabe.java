import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * konkrete Eingabeklasse fuer Dateien
 * @author philipp
 *
 */
public class DateiEingabe extends Eingabe {

 /**
  * Attribut fuer das Einlesen von Dateien (String mit Dateinamen)
  */
 private String fileName;

/**
 * Konstruktor fuer das Einlesen von Dateien
 * @param kennwertdarstellung
 * @param fileName
 */
 protected DateiEingabe(Kennwertdarstellung kennwertdarstellung, String fileName) {
  super(kennwertdarstellung);
  this.fileName = fileName;
  this.dateiNamenValidieren();
 }

 /**
  * Einlesen einer Datei
  */
 protected void einlesen() {
  File f = new File(this.fileName);
  try {
   Scanner sc = new Scanner(f);
   this.kennwertdarstellung.setStaaten(new ArrayList<Staat>());
   this.kennwertdarstellung.setNachbarschaften(new Nachbarschaften(null));
   
   /*
    * Validierung, ob KFZ-Kennzeichen Doppelunkt enthalten
    */
   while(sc.hasNextLine()) {
    String line = this.entferneKommentare(sc.nextLine());
    if(line.lastIndexOf(":") != line.indexOf(":")) {
     throw new IllegalArgumentException("Ungueltiges KFZ-Kennzeichen - KFZ-Kennzeichen duerfen keine Doppelpunkte enthalten. Bitte korrigieren Sie dies in der Eingabedatei.");
    }
   }
   
   /*
    * Neuer Scanner wird benoetigt nun zum Einlesen der Daten
    */
   sc.close();
   sc = new Scanner(f);
   while (sc.hasNextLine()) {
    /*
     * entferne Kommentare
     */
    String line = this.entferneKommentare(sc.nextLine());
    
    /*
     * ignoriere Leerzeilen
     */
    if (line.equals("")) {
     continue;
    }
    
    /*
     * erste Nichtleerzeile wird als Kennwertbezeichnung gewaehlt
     */
    if (this.kennwertdarstellung.getKennwertBezeichnung() == null) {
     this.kennwertdarstellung.setKennwertBezeichnung(line);
    } else {
     /*
      * ansonsten werden Nachbarschaftszeilen anhand des Doppelpunktes
      * von den Staatsdefinitionszeilen unterschieden,
      * wobei davon ausgegangen wird, dass die Staatszeilen
      * allesamt zuerst auftauchen (korrekte Syntax wird vorausgesetzt)
      */
     if (line.contains(":")) {
      this.nachbarschaftsZeileEinlesen(line);
     } else {
      this.staatenZeileEinlesen(line);
     }
    }
   }
   sc.close();
   
   /*
    * die Adjazenzmatrix ist hier nur null, wenn
    * es keine nachbarschaftsdefinitionen gab in der 
    * Eingabe
    */
   if (this.kennwertdarstellung.getNachbarschaften().getAdjazenzmatrix() == null) {
    int l = this.kennwertdarstellung.getStaaten().size();
    /*
     * default int-Werte in Java sind 0,
     * so entspricht folgende adjazenzmatrix
     * einer Matrix von keinen Nachbarschaften
     */
    this.kennwertdarstellung.setNachbarschaften(new Nachbarschaften(new int[l][l]));
   }   
  } catch (FileNotFoundException e) {
   throw new IllegalArgumentException(
     "Datei konnnte nicht gefunden werden. Bitte pruefen Sie, ob die Datei vorhanden ist.");
  }
 }

 /**
  * Einlesen einer Staatenzeile
  * @param line
  */
 private void staatenZeileEinlesen(String line) {
  ArrayList<Staat> staaten = this.kennwertdarstellung.getStaaten();
  String[] arr = line.replaceAll("\t", " ").split("\\s+");
  
  /*
   * Staat ist bereits in staaten-Liste (respektive dessen kfz)
   */
  if (this.indexVonKfzInStaatenListe(staaten, arr[0]) != -1) {
   throw new IllegalArgumentException(
     "Zwei Definitionszeilen mit der selben KFZ-Bezeichnung - es darf jede KFZ-Bezeichnung nur ein mal verwendet werden. Bitte korrigieren Sie dies in der Eingabedatei.");
  }
  
  String kfz = arr[0];
  double wert = Double.parseDouble(arr[1]);
  double laengengrad = Double.parseDouble(arr[2]);
  double breitengrad = Double.parseDouble(arr[3]);
  
  /*
   * Validierung auf ungueltige Werte
   */
  if(wert < 0) {
   throw new IllegalArgumentException("Ungueltiger Kennwert " + wert + " - Kennwerte muessen echt-positiv sein. Bitte korrigieren Sie dies in der Eingabedatei.");
  }
  // laengengrad -180 ist exklusiv des gueltigen wertebereiches
  if(laengengrad <= -180 || laengengrad > 180) {
   throw new IllegalArgumentException("Ungueltiger Laengengrad " + laengengrad + " - Laengengrade muessen jeweils im Bereich von -180 (exklusive) bis 180 liegen. Bitte korrigieren Sie dies in der Eingabedatei.");
  }
  if(breitengrad < -90 || breitengrad > 90) {
   throw new IllegalArgumentException("Ungueltiger Breitengrad " + breitengrad + " - Breitengrade muessen jeweils im Bereich von -90 bis 90 liegen. Bitte korrigieren Sie dies in der Eingabedatei.");
  }
  
  Staat staat = new Staat(kfz, wert, laengengrad, breitengrad);
  staaten.add(staat);
 }

 /**
  * Einlesen einer Nachbarschaftszeile
  * @param line
  */
 private void nachbarschaftsZeileEinlesen(String line) {
  ArrayList<Staat> staaten = this.kennwertdarstellung.getStaaten();
  int[][] adjazenzmatrix = this.kennwertdarstellung.getNachbarschaften().getAdjazenzmatrix();
  
  /*
   * wenn adjazenzmatrix noch nicht initialisiert ist, initialisieren
   */
  if (adjazenzmatrix == null) {
   int l = staaten.size();
   adjazenzmatrix = new int[l][l];
   this.kennwertdarstellung.setNachbarschaften(new Nachbarschaften(adjazenzmatrix));
  }
  
  /*
   * kfz vom Nachbar1 Land und dessen Nachbarn in zwei seperate strings bringen
   */
  int positionErsterDoppelpunkt = line.indexOf(":");  
  String kfz = line.substring(0, positionErsterDoppelpunkt).trim();
  String restZeile = line.substring(positionErsterDoppelpunkt + 1).trim();
  String[] nachbarn = restZeile.replaceAll("\t", " ").split("\\s+");
  
  /*
   * Leere Nachbarschaftsliste
   * Zeile ignorieren
   */
  if(nachbarn[0].equals("")) {
   return;
  }
  
  int i = this.indexVonKfzInStaatenListe(staaten, kfz);
  for (int j = 0; j < nachbarn.length; j++) {
   int k = this.indexVonKfzInStaatenListe(staaten, nachbarn[j]);

   /*
    * Adjazenzmatrix mit Nachbarschaftsinformation fuettern
    */
   adjazenzmatrix[i][k] = 1;
   adjazenzmatrix[k][i] = 1;
  }
 }
 
/**
 * Methode zum entfernen der Kommentare einer Zeile, die mit
 * # eingelaeutet werden
 * @param zeile
 * @return
 */
 private String entferneKommentare(String zeile) {
  int positionErsterRaute = zeile.indexOf("#");
  
  /*
   * gesamte Zeile ist Kommentar
   */
  if (positionErsterRaute == 0) {
   return "";
  }
  
  /*
   * Zeile besteht nur zum Teil aus Kommentar
   */
  if (positionErsterRaute >= 1) {
   zeile = zeile.substring(0, positionErsterRaute);
  }

  zeile = zeile.trim();
  return zeile;
 }

 /**
  * Methode zum Validieren des Dateinamen,
  * der mit .in oder .txt enden soll
  */
 private void dateiNamenValidieren() {
  if (!(this.fileName.endsWith(".in") || this.fileName.endsWith(".txt"))) {
   throw new IllegalArgumentException(
     "Dateiname endete nicht mit .in oder .txt. Dies wird jedoch vom Programm vorausgesetzt. Bitte passen Sie die Dateiendungen an.");
  }
 }

 /**
  * Methode zum Erhalten des numerischen Index eines Staaten in 
  * der staaten-Liste anhand seines kfz-Attributes
  * @param staaten
  * @param kfz
  * @return
  */
 private int indexVonKfzInStaatenListe(ArrayList<Staat> staaten, String kfz) {
  for (Staat s : staaten) {
   if (s.getKfz().equals(kfz)) {
    return staaten.indexOf(s);
   }
  }
  return -1;
 }
}
