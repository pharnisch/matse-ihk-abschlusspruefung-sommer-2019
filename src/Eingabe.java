
/**
 * abstrakte Oberklasse fuer Eingabe
 * @author philipp
 *
 */
public abstract class Eingabe {
 protected Kennwertdarstellung kennwertdarstellung;

 /**
  * Konstruktor
  * @param kennwertdarstellung
  */
 protected Eingabe(Kennwertdarstellung kennwertdarstellung) {
  this.kennwertdarstellung = kennwertdarstellung;
 }

 /**
  * Generischer Schnittpunkt zum Einlesen der Eingabedaten
  */
 abstract protected void einlesen();
}
