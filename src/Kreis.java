
/**
 * Kreis-Klasse
 * @author philipp
 *
 */
public class Kreis {
 private double r;
 private double x;
 private double y;

 /**
  * Konstruktor
  * @param r
  * @param x
  * @param y
  */
 public Kreis(double r, double x, double y) {
  this.r = r;
  this.x = x;
  this.y = y;
 }

 /**
  * Getter fuer den Radius
  * @return
  */
 public double getR() {
  return this.r;
 }

 /**
  * Getter fuer die x-Koordinate
  * @return
  */
 public double getX() {
  return this.x;
 }

 /**
  * Getter fuer die y-Koordinate
  * @return
  */
 public double getY() {
  return this.y;
 }

 /**
  * Setter fuer die x-Koordinate
  * @return
  */
 public void setX(double x) {
  this.x = x;
 }

 /**
  * Setter fuer die y-Koordinate
  * @return
  */
 public void setY(double y) {
  this.y = y;
 }

 /**
  * Setter fuer den Radius
  * @param r
  */
 public void setR(double r) {
  this.r = r;
 }
}
