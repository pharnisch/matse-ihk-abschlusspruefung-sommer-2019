import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;
import java.lang.StringBuilder;
import java.io.FileNotFoundException;

/**
 * Hauptprogramm der Grossen Programmieraufgabe
 * 
 * @author Philipp Lars Harnisch
 */
public class Hauptprogramm {

 /**
  * statische main-Methode, welche das Programm startet und das Konsolenargument
  * verarbeitet
  * 
  * @param args relativer Pfad zu der Eingabedatei (erstes Argument)
  */
 public static void main(String[] args) {
  if (args.length == 0) {
   System.out.println("Es wurde kein Argument fuer den Dateinamen der Eingabedatei uebergeben. Bitte Dateinamen angeben, um Programm verwenden zu koennen.");
  }

  String fileName = args[0];
  System.out.println("Programmausfuehrung mit: " + fileName.substring(1));

  Kennwertdarstellung kd = new Kennwertdarstellung(fileName);
  kd.hauptRoutine();

 }
}
