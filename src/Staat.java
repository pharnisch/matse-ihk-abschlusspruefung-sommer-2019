
/**
 * Klasse zur Repraesentation eines Staates
 * @author philipp
 *
 */
public class Staat {
 private String kfz;
 private Kreis kreis;

 /**
  * Konstruktor
  * @param kfz
  * @param wert
  * @param laengengrad
  * @param breitengrad
  */
 public Staat(String kfz, double wert, double laengengrad, double breitengrad) {
  if(wert < 0) {
   throw new IllegalArgumentException("Negativer Wert enthalten - diese sind nicht erlaubt.");
  }
  
  this.kfz = kfz;
  this.kreis = new Kreis(Math.sqrt(wert / Math.PI), laengengrad, breitengrad);
 }

 /**
  * Getter fuer den KFZ-Kennzeichen-String
  * @return
  */
 public String getKfz() {
  return this.kfz;
 }

 /**
  * Getter fuer die x-Koordinate
  * @return
  */
 public double getX() {
  return this.kreis.getX();
 }

 /**
  * Getter fuer die y-Koordinate
  * @return
  */
 public double getY() {
  return this.kreis.getY();
 }

 /**
  * Getter fuer den Radius
  * @return
  */
 public double getR() {
  return this.kreis.getR();
 }

 /**
  * Setter fuer den Radius
  * @param r
  */
 public void setR(double r) {
  this.kreis.setR(r);
 }
 
 /**
  * Setter fuer die x-Koordinate
  * @param x
  */
 public void setX(double x) {
  this.kreis.setX(x);
 }

 /**
  * Setter fuer die y-Koordinate
  * @param y
  */
 public void setY(double y) {
  this.kreis.setY(y);
 }

 /**
  * to-String-Methode zur Stringdarstellung einer Klasseninstanz
  */
 public String toString() {
  return this.kfz + ", x: " + this.getX() + ", y: " + this.getY() + ", r: " + this.getR();
 }
}
