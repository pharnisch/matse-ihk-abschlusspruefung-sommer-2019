
/**
 * Klasse zur Repraesentation eines Vektors
 * @author philipp
 *
 */
public class Vektor {
 private double x;
 private double y;

 /**
  * Konstruktor
  * @param x
  * @param y
  */
 public Vektor(double x, double y) {
  this.x = x;
  this.y = y;
 }

 /**
  * Getter fuer x-Koordinate
  * @return
  */
 public double getX() {
  return this.x;
 }
 
 /**
  * Setter fuer x-Koordinate
  * @param x
  */
 public void setX(double x) {
  this.x = x;
 }

 /**
  * Getter fuer y-Koordinate
  * @return
  */
 public double getY() {
  return this.y;
 }
 
 /**
  * Setter fuer y-Koordinate
  * @param y
  */
 public void setY(double y) {
  this.y = y;
 }

 /**
  * Methode zum Skalieren des Vektors auf eine gewuenschte Laenge d
  * @param d
  */
 public void skalieren(double d) {
  double s = d / this.getNorm();
  this.x *= s;
  this.y *= s;
 }

 /**
  * Methode zum Berechnen der Laenge des Vektors
  * @return
  */
 public double getNorm() {
  return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
 }

 /**
  * to-String-Methode zur Stringdarstellung einer Klasseninstanz
  */
 public String toString() {
  return "Vektor: (" + String.format("%.2f", this.x) + " | " + String.format("%.2f", this.y) + ")";
 }
}
