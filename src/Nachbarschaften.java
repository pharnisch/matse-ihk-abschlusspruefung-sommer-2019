
/**
 * Klasse fuer die Abbildung von Nachbarschaften zwischen Staaten
 * @author philipp
 *
 */
public class Nachbarschaften {
 /**
  * Adjazenzmatrix von den Indizes der Staaten
  */
 private int[][] adjazenzmatrix;

 /**
  * Konstruktor
  * @param a
  */
 public Nachbarschaften(int[][] a) {
  this.adjazenzmatrix = a;
 }

 /**
  * Methode zum Pruefen, ob Staaten benachbart sind
  * @param a
  * @param b
  * @return
  */
 public boolean isNachbar(int a, int b) {
  /*
   * welcher Index Zeile und Spalte ist, ist egal, da
   * eine Adjazenzmatrix symmetrisch ist
   */
  return this.adjazenzmatrix[a][b] == 1;
 }

 /**
  * Getter fuer die Adjazenzmatrix
  * @return
  */
 public int[][] getAdjazenzmatrix(){
  return this.adjazenzmatrix;
 }
 
 /**
  * to-String-Methode zur Stringdarstellung einer Klasseninstanz
  */
 public String toString() {
  String s = "";
  for (int[] i : this.adjazenzmatrix) {
   for (int j : i) {
    s += j + " ";
   }
   s += "\n";
  }
  return s;
 }
}
