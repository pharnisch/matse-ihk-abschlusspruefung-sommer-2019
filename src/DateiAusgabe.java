import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * konkrete Ausgabeklasse fuer Dateien
 * @author philipp
 *
 */
public class DateiAusgabe extends Ausgabe {

 /**
  * Attribut fuer das Schreiben der Ergebnisse in eine Datei (String mit
  * Dateinamen)
  */
 private String fileName;

 /**
  * Konstruktor fuer das Schreiben der Ergebnisse in eine Datei
  * 
  * @param fileName
  * @param kennwertdarstellung
  */
 public DateiAusgabe(Kennwertdarstellung kennwertdarstellung, String fileName) {
  super(kennwertdarstellung);
  this.fileName = fileName;
 }

 /**
  * Schreiben der Ergebnisse in die Datei
  */
 public void ausgeben() {
  /*
   * Entfernen der ehemaligen Dateiendung
   */
  String fileNameOutput;
  if (fileName.substring(fileName.length() - 3).equals(".in")) {
   fileNameOutput = fileName.substring(9, fileName.length() - 3);
  } else if (fileName.substring(fileName.length() - 4).equals(".txt")) {
   fileNameOutput = fileName.substring(9, fileName.length() - 4);
  } else {
   throw new IllegalArgumentException(
     "Fehler beim Schreiben des Ergebnisses: Eingabedatei endet nicht mit .in oder .txt.");
  }

  /*
   * Die Pfade werden zur Vereinfachung abgeflacht, sodass im output-Ordner alle
   * Dateien nebeneinander liegen
   */
  fileNameOutput = fileNameOutput.replace("/", "-");
  File f = new File("../output/" + fileNameOutput + ".out");

  /*
   * Schreiben der Ausgaben in das Dateisystem
   */
  try {
   PrintWriter pw = new PrintWriter(f);
   pw.write(this.berechneAusgabe());
   pw.close();
  } catch (FileNotFoundException e) {
   System.out.println(e.getMessage());
  }
 }

}
