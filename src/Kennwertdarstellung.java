import java.util.ArrayList;
import java.util.Arrays;

/**
 * Zentrale Klasse der Kennwertdarstellung
 * von Laendern in Bezug auf beliebig grosse
 * nicht-negative Kennwerte, wobei versucht wird
 * die Lagebeziehungen der Laender beizubehalten
 * @author philipp
 *
 */
public class Kennwertdarstellung {
 private Eingabe eingabe;
 private Ausgabe ausgabe;
 private String kennwertBezeichnung;
 private ArrayList<Staat> staaten;
 private Nachbarschaften nachbarschaften;
 private int iteration;

 /**
  * Konstruktor
  * @param fileName
  */
 public Kennwertdarstellung(String fileName) {
  this.eingabe = new DateiEingabe(this, fileName);
  this.ausgabe = new DateiAusgabe(this, fileName);
 }

 /**
  * Methode zum Ausfuehren der Hauptfunktionalitaeten
  */
 public void hauptRoutine() {
  try {
   this.eingabe.einlesen();

   this.auflockern();
   for (int i = 0; i < 1000; i++) {
    Vektor[][] alphas = this.berechneAlphas();
    Vektor[][] betas = this.berechneBetas();
    if(this.isKonvergent(alphas, betas)) {
     break;
    }
    this.verschiebeMittelpunkte(alphas, betas);
    this.setIteration(i + 1);
   }

   this.ausgabe.ausgeben();
  } catch(Exception e) {
   System.out.println(e.getMessage());
  }
 }

 /**
  * Hilfsmethode zum Pruefen, ob weitere Iterationen
  * beim Verschieben von Mittelpunkten der Staatskreise
  * erforderlich sind oder nicht
  * @param alphas
  * @param betas
  * @return
  */
 private boolean isKonvergent(Vektor[][] alphas, Vektor[][] betas) {
  double epsilon = Math.pow(10, -2);
  int l = alphas.length;
  int staatenKonvergent = 0;
  
  for(int i=0; i<l; i++) {
   double insgesamteVerschiebung = 0;
   for(int j=0; j<l; j++) {
    insgesamteVerschiebung += alphas[i][j].getNorm() + betas[i][j].getNorm();
   }
   
   /*
    * wenn mittlere Verschiebungslaenge eines Staates kleiner ist als epsilon,
    * gilt er als konvergent
    */
   if(insgesamteVerschiebung/(double)l < epsilon) {
    staatenKonvergent++;
   }
  }
  /*
   * wenn alle Staaten konvergent sind, zaehlt der Prozess insgesamt als konvergent
   */
  return staatenKonvergent == l;
 }

 /**
  * Hilfsmethode zum Vorskalieren der x- und y-Koordinaten,
  * damit die Staatskreise sich nicht mehr oder nicht mehr
  * so stark ueberlappen
  */
 private void auflockern() {
  int l = this.staaten.size();
  double maxRadius = Double.NEGATIVE_INFINITY;
  for(int i=0; i<l; i++) {
   if(staaten.get(i).getR() > maxRadius) {
    maxRadius = staaten.get(i).getR();
   }
  }
  
  double skalierungsFaktor = 3/maxRadius;
  for (Staat staat : this.staaten) {
   double skalierterRadius = staat.getR()*skalierungsFaktor;
   staat.setR(skalierterRadius);
  }
 }

 /**
  * Hilfsmethode zum Berechnen der Abstossungskraefte
  * in Vektoren
  * @return
  */
 private Vektor[][] berechneAlphas() {
  int l = this.staaten.size();
  Vektor[][] alphas = new Vektor[l][l];
  for (int i = 0; i < l; i++) {
   for (int j = 0; j < l; j++) {
    /*
     * Staaten sollen sich nicht selbst
     * zum Verschieben veranlassen
     */
    if (i == j) {
     alphas[i][j] = new Vektor(0, 0);
     continue;
    }
    
    Vektor alpha = new Vektor(
      this.staaten.get(i).getX() - this.staaten.get(j).getX(),
      this.staaten.get(i).getY() - this.staaten.get(j).getY()
      );
    
    /*
     * Mittelpunkte von zwei verschiedenen Staaten sind
     * gleich - ersten direkt um einen Laengengrad verschieben
     * weiterhin muss dann der Vektor von oben angepasst werden
     */
    if(alpha.getX() == 0 && alpha.getY() == 0) {
     double x = this.staaten.get(i).getX();
     this.staaten.get(i).setX(x + 1);
     alpha.setX(1);
    }
    double a = this.staaten.get(i).getR();
    double b = this.staaten.get(j).getR();
    double d = alpha.getNorm();

    /*
     * Die Summe der Radien ist kleiner gleich der Distanz der beiden Mittelpunkte -
     * das bedeutet, dass die Kreis- raender einen Abstand zueinander haben und
     * somit keine Ueberlappung der Kreise vorliegt, daher gibt es auch keinen
     * Abstossungsvektor alpha
     */
    if (a + b <= d) {
     alphas[i][j] = new Vektor(0, 0);
     continue;
    }
    
    /*
     * skaliere Richtungsvektor auf die Laenge
     * der Ueberschneidungslaenge
     */
    double skalar = Math.abs(d - a - b);
    alpha.skalieren(skalar);
    alphas[i][j] = alpha;
   }
  }
  return alphas;
 }

 /**
  * Hilfsmethode zum Berechnen der Anziehungskraefte
  * in Vektoren
  * @return
  */
 private Vektor[][] berechneBetas() {
  int l = this.staaten.size();
  Vektor[][] betas = new Vektor[l][l];
  
  for (int i = 0; i < l; i++) {
   for (int j = 0; j < l; j++) {
    
    /*
     * Staaten sollen sich selbst nicht verschieben
     */
    if (i == j) {
     betas[i][j] = new Vektor(0, 0);
     continue;
    }
    
    /*
     * Staaten sind keine Nachbarn und daher gibt es pauschal keinen
     * Anziehungsvektor beta
     */
    if (!this.isNachbar(i, j)) {
     betas[i][j] = new Vektor(0, 0);
     continue;
    }
    
    Vektor beta = new Vektor(
      this.staaten.get(j).getX() - this.staaten.get(i).getX(),
      this.staaten.get(j).getY() - this.staaten.get(i).getY());
    double laenge = beta.getNorm();
    double abstand = laenge - this.staaten.get(i).getR() - this.staaten.get(j).getR();
    
    /*
     * Es gibt keinen Abstand zwischen den Staatskreisen, daher gibt es auch keine
     * Anziehungskraft beta
     */
    if (abstand <= 0) {
     betas[i][j] = new Vektor(0, 0);
    } else {
     /*
      * Es gibt einen Abstand zwischen den Staatskreisen, daher gibt es auch eine
      * Anziehungskraft beta, welche auf die Laenge des Abstandes der Kreisraender
      * skaliert wird
      */
     beta.skalieren(abstand);
     betas[i][j] = beta;
     }
    }
  }
  return betas;
 }

 /**
  * Methode zum tatsaechlichen Verschieben der 
  * Mittelpunkte aller Staaten
  * @param alphas
  * @param betas
  */
 private void verschiebeMittelpunkte(Vektor[][] alphas, Vektor[][] betas) {
  double alpha_gewicht = 0.5;
  double beta_gewicht = 0.5;
  int l = this.staaten.size();
  
  for (int i = 0; i < l; i++) {
   Staat staat = this.staaten.get(i);
   double summe_x_verschiebung = 0;
   double summe_y_verschiebung = 0;
   int anzahlRelevanterVerschiebungen = 0;
   
   for (int j = 0; j < l; j++) {
    boolean relevant = alphas[i][j].getX() != 0 || alphas[i][j].getY() != 0 || betas[i][j].getX() != 0 || betas[i][j].getY() != 0;
    if(relevant) {
     anzahlRelevanterVerschiebungen++;
     summe_x_verschiebung += alphas[i][j].getX() * alpha_gewicht + betas[i][j].getX() * beta_gewicht;
     summe_y_verschiebung += alphas[i][j].getY() * alpha_gewicht + betas[i][j].getY() * beta_gewicht;
    }
   }
   
   /*
    * Durchschnittliche Verschiebung berechnen
    * und Staat um diese verschieben
    */
   staat.setX(staat.getX() + summe_x_verschiebung / (double) anzahlRelevanterVerschiebungen);
   staat.setY(staat.getY() + summe_y_verschiebung / (double) anzahlRelevanterVerschiebungen);
  }
 }

 /**
  * Hilfsmethode zum Pruefen,
  * ob Staat mit Index a
  * und Staat mit Index b
  * Nachbarn sind
  * @param a
  * @param b
  * @return
  */
 public boolean isNachbar(int a, int b) {
  return this.nachbarschaften.isNachbar(a, b);
 }

 /**
  * Setter fuer die Kennwertbezeichnung
  * @param kennwertBezeichnung
  */
 public void setKennwertBezeichnung(String kennwertBezeichnung) {
  this.kennwertBezeichnung = kennwertBezeichnung;
 }
 
 /**
  * Getter fuer die Kennwertbezeichnung
  * @return
  */
 public String getKennwertBezeichnung() {
  return this.kennwertBezeichnung;
 }

 /**
  * Setter fuer die Staatenliste
  * @param staaten
  */
 public void setStaaten(ArrayList<Staat> staaten) {
  this.staaten = staaten;
 }

 /**
  * Getter fuer die Staatenliste
  * @return
  */
 public ArrayList<Staat> getStaaten() {
  return this.staaten;
 }

 /**
  * Setter fuer das Nachbarschaftenattribut
  * @param nachbarschaften
  */
 public void setNachbarschaften(Nachbarschaften nachbarschaften) {
  this.nachbarschaften = nachbarschaften;
 }
 
 /**
  * Getter fuer das Nachbarschaftenattribut
  * @param nachbarschaften
  */
 public Nachbarschaften getNachbarschaften() {
  return this.nachbarschaften;
 }

 /**
  * Setter fuer das Iterationen-Attribut
  * @param i
  */
 public void setIteration(int i) {
  this.iteration = i;
 }

 /**
  * Getter fuer das Iterationen-Attribut
  * @param i
  */
 public int getIteration() {
  return this.iteration;
 }
}
