import java.util.ArrayList;

/**
 * abstrakte Ausgabe-Klasse
 * @author philipp
 *
 */
public abstract class Ausgabe {
 private Kennwertdarstellung kennwertdarstellung;

 /**
  * Konstruktor
  * @param kennwertdarstellung
  */
 public Ausgabe(Kennwertdarstellung kennwertdarstellung) {
  this.kennwertdarstellung = kennwertdarstellung;
 }

 /**
  * Generischer Schnittpunkt zum Ausgeben der Ergebnisse
  */
 public abstract void ausgeben();

 /**
  * Hilfsmethode zum Berechnen der Ausgabegrenzen
  * @return double-Array
  */
 private double[][] berechneQuadrat() {
  double[][] arr = new double[2][2];
  double x_min = Double.POSITIVE_INFINITY;
  double x_max = Double.NEGATIVE_INFINITY;
  double y_min = Double.POSITIVE_INFINITY;
  double y_max = Double.NEGATIVE_INFINITY;
  for (Staat staat : this.kennwertdarstellung.getStaaten()) {
   if (x_min > staat.getX() - staat.getR()) {
    x_min = staat.getX() - staat.getR();
   }
   if (x_max < staat.getX() + staat.getR()) {
    x_max = staat.getX() + staat.getR();
   }
   if (y_min > staat.getY() - staat.getR()) {
    y_min = staat.getY() - staat.getR();
   }
   if (y_max < staat.getY() + staat.getR()) {
    y_max = staat.getY() + staat.getR();
   }
  }
  if (x_max - x_min >= y_max - y_min) {
   double x_y_diff = (x_max - x_min) - (y_max - y_min);
   y_max += x_y_diff/2;
   y_min -= x_y_diff/2;
  } else {
   double x_y_diff = (y_max - y_min) - (x_max - x_min);
   x_max += x_y_diff/2;
   x_min -= x_y_diff/2;
  }
  arr[0][0] = x_min;
  arr[0][1] = x_max;
  arr[1][0] = y_min;
  arr[1][1] = y_max;
  return arr;
 }

 /**
  * Methode zum berechnen des Ausgabe-Strings
  * @return ausgabe-String
  */
 protected String berechneAusgabe() {
  double[][] quadrat = this.berechneQuadrat();
  String s = "reset\nset xrange [" + quadrat[0][0] + ":" + quadrat[0][1] + "]\n";
  s += "set yrange [" + quadrat[1][0] + ":" + quadrat[1][1] + "]\n";
  s += "set size ratio 1.0\nset title \"" + this.kennwertdarstellung.getKennwertBezeichnung() + ", Iterationen: "
    + this.kennwertdarstellung.getIteration() + "\"\n";
  s += "unset xtics\nunset ytics\n$data << EOD\n";
  ArrayList<Staat> staaten = this.kennwertdarstellung.getStaaten();
  for (int i = 0; i < staaten.size(); i++) {
   s += staaten.get(i).getX() + " " + staaten.get(i).getY() + " " + staaten.get(i).getR() + " "
     + staaten.get(i).getKfz() + " " + i + "\n";
  }
  s += "EOD\nplot \\\n'$data' using 1:2:3:5 with circles lc var notitle, \\\n";
  s += "'$data' using 1:2:4:5 with labels font \"arial,9\" tc var notitle";
  return s;
 }
}
