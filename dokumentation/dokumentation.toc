\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Juristische Erkl\IeC {\"a}rung}{4}
\contentsline {section}{\numberline {2}Aufgabenbeschreibung und -analyse}{5}
\contentsline {section}{\numberline {3}Programmbeschreibung}{6}
\contentsline {subsection}{\numberline {3.1}\IeC {\"A}nderungen zum Konzepttag}{6}
\contentsline {subsection}{\numberline {3.2}Struktogramme}{8}
\contentsline {subsubsection}{\numberline {3.2.1}Kontrolle}{8}
\contentsline {subsubsection}{\numberline {3.2.2}Eingabe}{10}
\contentsline {subsubsection}{\numberline {3.2.3}Voriteration}{15}
\contentsline {subsubsection}{\numberline {3.2.4}Berechnen der Anziehungs- und Absto\IeC {\ss }ungskr\IeC {\"a}fte}{16}
\contentsline {subsubsection}{\numberline {3.2.5}Verschiebung der Mittelpunkte}{21}
\contentsline {subsubsection}{\numberline {3.2.6}Ausgabe}{21}
\contentsline {subsubsection}{\numberline {3.2.7}Sonstiges}{24}
\contentsline {subsection}{\numberline {3.3}UML-Klassendiagramm}{25}
\contentsline {subsection}{\numberline {3.4}UML-Sequenzdiagramm}{27}
\contentsline {section}{\numberline {4}Testf\IeC {\"a}lle}{29}
\contentsline {subsection}{\numberline {4.1}\IeC {\"A}quivalenzklassen}{29}
\contentsline {subsection}{\numberline {4.2}Normalf\IeC {\"a}lle}{29}
\contentsline {subsubsection}{\numberline {4.2.1}Fl\IeC {\"a}che Deutschland und Nachbarstaaten (Aufgabenstellung)}{29}
\contentsline {subsubsection}{\numberline {4.2.2}Bierkonsum Deutschland und Nachbarstaaten (Aufgabenstellung)}{31}
\contentsline {subsubsection}{\numberline {4.2.3}Fl\IeC {\"a}che Staaten Mitteleuropas (Aufgabenstellung)}{33}
\contentsline {subsection}{\numberline {4.3}Sonderf\IeC {\"a}lle}{36}
\contentsline {subsubsection}{\numberline {4.3.1}Leerzeilen und Kommentare}{36}
\contentsline {subsubsection}{\numberline {4.3.2}Leerzeichen und Tabulatoren}{38}
\contentsline {subsubsection}{\numberline {4.3.3}Zwei Staaten auf selbem Mittelpunkt}{40}
\contentsline {subsubsection}{\numberline {4.3.4}Leere Nachbarschaftsliste}{42}
\contentsline {subsubsection}{\numberline {4.3.5}Keine Nachbarschaftsdefinitionszeilen}{44}
\contentsline {subsubsection}{\numberline {4.3.6}Sehr kleine Werte}{45}
\contentsline {subsubsection}{\numberline {4.3.7}Sehr gro\IeC {\ss }e Werte}{47}
\contentsline {subsection}{\numberline {4.4}Fehlerf\IeC {\"a}lle}{49}
\contentsline {subsubsection}{\numberline {4.4.1}Doppelpunkt in KFZ-Kennzeichen enthalten}{49}
\contentsline {subsubsection}{\numberline {4.4.2}L\IeC {\"a}ngengrad -180}{50}
\contentsline {subsubsection}{\numberline {4.4.3}L\IeC {\"a}ngengrad gr\IeC {\"o}\IeC {\ss }er 180}{51}
\contentsline {subsubsection}{\numberline {4.4.4}Breitengrad kleiner -90}{51}
\contentsline {subsubsection}{\numberline {4.4.5}Breitengrad gr\IeC {\"o}\IeC {\ss }er 90}{52}
\contentsline {subsubsection}{\numberline {4.4.6}negativer Kennwert}{53}
\contentsline {section}{\numberline {5}Anwendung (Benutzeranleitung)}{55}
\contentsline {subsection}{\numberline {5.1}Eingabedatei}{55}
\contentsline {subsubsection}{\numberline {5.1.1}KFZ-Kennzeichen}{55}
\contentsline {subsubsection}{\numberline {5.1.2}Kennwert}{55}
\contentsline {subsubsection}{\numberline {5.1.3}Breitengrad}{55}
\contentsline {subsubsection}{\numberline {5.1.4}L\IeC {\"a}ngengrad}{55}
\contentsline {subsubsection}{\numberline {5.1.5}Nachbarschaftsliste}{56}
\contentsline {subsection}{\numberline {5.2}Graphische Darstellung mit gnuplot}{56}
\contentsline {subsection}{\numberline {5.3}Details zu Programmiersprache und Hauptprogramm}{56}
\contentsline {subsection}{\numberline {5.4}Details f\IeC {\"u}r das Testen}{56}
\contentsline {subsection}{\numberline {5.5}Fehlermeldungen}{57}
\contentsline {section}{\numberline {6}Sprache und Umgebung}{58}
\contentsline {subsection}{\numberline {6.1}Wiederverwendbare Klassen und offene Schnittstellen}{58}
\contentsline {subsection}{\numberline {6.2}Exceptions}{58}
\contentsline {section}{\numberline {7}Zusammenfassung und Ausblick}{59}
\contentsline {section}{\numberline {8}Anhang}{60}
\contentsline {subsection}{\numberline {8.1}Quellcode}{60}
\contentsline {subsubsection}{\numberline {8.1.1}Hauptprogramm}{60}
\contentsline {subsubsection}{\numberline {8.1.2}Eingabe}{69}
\contentsline {subsubsection}{\numberline {8.1.3}Ausgabe}{76}
\contentsline {subsubsection}{\numberline {8.1.4}Hilfsklassen}{80}
\contentsline {subsection}{\numberline {8.2}Test-Skript}{87}
