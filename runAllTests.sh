#!/bin/sh

# Diese Datei dient zum Ausfuehren von allen Testfaellen im Verzeichnis "input" oder beliebig benannten Unterverzeichnissen.
# Die Testfaell muessen auf .in enden und die Resultate werden im gleichnamige Dateien im Verzeichnis "output" und der Endung .out niedergeschrieben.

TEST_FOLDER="input"
SOURCE_FOLDER="src"
HAUPTDATEI_KOMPILIERT_WITHOUT_SUFFIX="Hauptprogramm"
HAUPTDATEI="$HAUPTDATEI_KOMPILIERT_WITHOUT_SUFFIX.java"
HAUPTDATEI_KOMPILIERT="$HAUPTDATEI_KOMPILIERT_WITHOUT_SUFFIX.class"

if [ ! -f "$SOURCE_FOLDER/$HAUPTDATEI" ]
then
  echo "##### Die Hauptdatei ($SOURCE_FOLDER/$HAUPTDATEI) konnte nicht gefunden werden."
  exit 1
else
  echo "##### Kompiliere Hauptdatei ($SOURCE_FOLDER/$HAUPTDATEI) [...]"
  echo
  cd $SOURCE_FOLDER
  javac $HAUPTDATEI
  cd ..
fi

if [ ! -f "$SOURCE_FOLDER/$HAUPTDATEI_KOMPILIERT" ]
then
  echo "##### Die kompilierte Hauptdatei ($SOURCE_FOLDER/$HAUPTDATEI_KOMPILIERT) konnte nicht gefunden werden."
  exit 1
else
  echo "##### Fuehre mit den Dateien im Verzeichnis input die Hauptdatei aus ($SOURCE_FOLDER/$HAUPTDATEI_KOMPILIERT) [...]"
  echo
  cd $SOURCE_FOLDER
  # Verwende alle .in-Dateien im Ordner "input"
  find ../input -type f -name "*.in" -or -name "*.txt" | while read FILE_NAME; do
    [ -e "$FILE_NAME" ] || continue
    java $HAUPTDATEI_KOMPILIERT_WITHOUT_SUFFIX "$FILE_NAME"
    echo
  done
  cd ..
fi
